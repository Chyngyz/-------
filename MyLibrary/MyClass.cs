﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    public class MyClass
    {
        private double sum;
        private double pro;
        private int srok;

        public MyClass(double sum,double pro,int srok)
        {
            this.sum=sum;
            this.pro=pro;
            this.srok =srok;

        }
        public double[] Raschet()
        {
            double[] arr = new double[srok];
            arr[0] = sum;

            for (int i = 1; i < srok; i++)

            {
                double c = sum * (pro / srok);
                arr[i] = arr[i - 1] + c;
                arr[i] = sum +c*(i+ 1);
            }
            return arr;
        }
    }
}
